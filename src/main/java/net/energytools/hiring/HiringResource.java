package net.energytools.hiring;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ArrayBlockingQueue;
import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping(value = "/api/hiring", produces = MediaType.APPLICATION_JSON_VALUE)
public class HiringResource {

    private BlockingQueue<HttpServletRequest> requestsQueue = new ArrayBlockingQueue<HttpServletRequest>(1);
    private int counter = 0;
    private final Logger log = LoggerFactory.getLogger(this.getClass());


    @RequestMapping(value = "/counter", method = RequestMethod.GET)
    public ResponseEntity<CounterDTO> getCounter(HttpServletRequest request) throws InterruptedException {
        requestsQueue.put(request);
        ResponseEntity<CounterDTO> response = this.processResponse(request);
        this.requestsQueue.take();
        return response;
    }

    private ResponseEntity<CounterDTO> processResponse(HttpServletRequest request) {
        String requestType = request.getHeader("X-Request-Type");
        if (requestType != null){
            if(requestType.toLowerCase().equals("a")) {
                
                if(counter%3 == 0){
                    log.info("Request of type A {}", counter);
                    counter+=1;  
                    try {
                        Thread.sleep(2000);
                    } catch (InterruptedException ignored) {}
                } 
            }
            else if(requestType.toLowerCase().equals("b")) {
                 if(counter%3 == 1){
                    log.info("Request of type B {}", counter);
                    counter+=1;  
                 } 
            }
            else if(requestType.toLowerCase().equals("c")) {
                if(counter%3 == 2){
                    log.info("Request of type C {}", counter);
                    counter+=1;  
                } 
            }
            else{
                log.info("Other type of request", counter);
            }

        }  else {
            log.info("Other type of request {}", counter);
        }
        return ResponseEntity.ok(new CounterDTO(counter));
    }

}
