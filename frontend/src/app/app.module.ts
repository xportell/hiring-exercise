import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { MultiRequestComponent } from './multiRequest/multiRequest.component';
import { D3Module } from '../d3/d3.module';

@NgModule({
  declarations: [
    AppComponent,
    MultiRequestComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    D3Module
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
