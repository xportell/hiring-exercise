import { Component, OnInit, EventEmitter, AfterViewInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, from, interval, timer } from 'rxjs';
import { mergeMap, take } from 'rxjs/operators';
import { Node } from '../../d3/models/node';
import { Link } from '../../d3/models/link';

interface ICounterDTO {
    value: number;
}

@Component({
  selector: 'app-multi-request',
  templateUrl: './multiRequest.component.html',
  styleUrls: ['./multiRequest.component.css']
})
export class MultiRequestComponent implements OnInit {
    private backendUrl = 'http://localhost:8080/api/hiring/counter';
    public lastValue = 0;
    private contentTypes = ['A', 'B', 'C'];
    private values: Array<number> = [];
    private startStamp = 0;
    private intervalStamp = null;
    private nodes: Node[] = [];
    private links: Link[] = [];
    private refresh: EventEmitter<any> = new EventEmitter();
    private stampNode = null;

    constructor(private http: HttpClient) {
        this.nodes.push(new Node(0, 'Loading...'));
        this.nodes[0].disabled = true;
    }

    ngOnInit() {
        this.generateFetch('').subscribe(
            response => {
                this.fetchValue();
            },
            error => {
                console.warn('Error: is back-end started?');
                this.fetchValue();
            },
        );
    }

    fetchValue(): void {
        this.values = [];
        this.startTimer();
        this.nodes[0].disabled = true;
        this.nodes[0].value = 'Loading...';
        this.stampNode = this.addNode(this.startStamp.toString() + 'ms');

        interval(100).pipe(take(this.contentTypes.length), mergeMap(i => this.generateFetch(this.contentTypes[i]))).subscribe(
                (result: ICounterDTO) => {
                    this.values.push(result.value);
                    this.addNode(this.contentTypes[(result.value - 1) % 3] + ': ' + result.value.toString());
                },
                error => {
                    console.warn('Error', error);
                    const s = ['Error', 'Try', 'Start', 'Backend'];
                    while ( (this.nodes.length - 1 ) % 4 !== 0 ) {
                        const mod = (this.nodes.length - 1 ) % 4;
                        this.addNode((s[mod]));
                    }
                    this.stampNode.value = 'Error!';
                    this.stopTimer();
                },
                () =>  this.stopTimer()
        );
    }

    generateFetch(contentType: string = 'A'): Observable<Object> {
        if ( !contentType) {
            return this.http.get(this.backendUrl);
        }
        const options = { headers: new HttpHeaders().set('X-Request-Type', contentType) };
        return this.http.get(this.backendUrl + '/?' + contentType , options);
    }

    startTimer() {
        this.startStamp = Date.now();
        this.intervalStamp = setInterval(() => { // Interval to increment timestamp
            this.stampNode.value = ((Date.now() - this.startStamp)) + 'ms';
        }, 3);
    }

    stopTimer() {
        clearInterval(this.intervalStamp); // Reset interval
        this.intervalStamp = null;
        this.startStamp = 0;
        this.stampNode = null;
        setTimeout( () => { // Delay root node clickable
            this.nodes[0].disabled = false;
            this.nodes[0].value = 'Click me!';
        }, 400);
    }

    addNode(value: string) {
        const index = this.nodes.length;
        const mod = (index - 1) % 4;
        const lastStampIndex = index - mod;
        this.nodes.push(new Node(this.nodes.length, value));
        if ( !mod ) {
            this.links.push(new Link(this.nodes[index], this.nodes[0]));
        } else  {
            this.links.push(new Link(this.nodes[index], this.nodes[lastStampIndex]));
        }
        this.refresh.next('refresh');
        return this.nodes[this.nodes.length - 1];
    }

    nodeClick(node) {
        if (node.disabled) {
            return;
        }
        if (node.index === 0) {
            this.fetchValue(); // If the root node is active request next content
        }
    }
}
