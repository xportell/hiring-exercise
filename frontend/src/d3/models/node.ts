import * as d3 from 'd3';
import * as d3Force from 'd3-force';

const SPECTRUM = {
  base: '#ecd932',
  stamp: '#2196F3',
  leaf: '#ff6347'
};

export class Node implements d3.SimulationNodeDatum {
    // optional - defining optional implementation properties - required for relevant typing assistance
    index?: number;
    x?: number;
    y?: number;
    vx?: number;
    vy?: number;
    fx?: number | null;
    fy?: number | null;
    id: string;
    value: string;
    type = 'leaf';
    disabled = false;

    constructor(id, value = 'Node') {
      this.id = id;
      this.index = id;
      this.value = value;
      this.setType();
    }

    setType() {
      if (!this.index) {
        this.type = 'base';
      } else if ( (this.index - 1) % 4 ) {
        this.type = 'stamp';
      }

      this.setXY();
    }

    getType() {
      return this.type;
    }

    setXY() {
      if (this.type !== 'base') {
        const a = (Math.trunc(this.index - 1) / 4) * 90;
        this.x = 30 * Math.cos(a);
        this.y = 30 * Math.sin(a);
      }
    }


    get r() {
      return 45;
    }

    get fontSize() {
      return 20;
    }

    get color() {
      return SPECTRUM[this.type];
    }
  }
