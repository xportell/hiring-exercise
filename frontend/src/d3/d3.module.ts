import { NgModule } from '@angular/core';

import { CommonModule } from '@angular/common';


import { GraphComponent  } from './components/graph.component';
import { NodeVisualComponent } from './components/node-visual.component';
import { LinkVisualComponent  } from './components/link-visual.component';
import { DraggableDirective  } from './directives/draggable.directive';
import { ZoomableDirective } from './directives/zoomable.directive';
import { D3Service } from './providers/d3.service';


@NgModule({
    imports:      [ CommonModule ],
    declarations: [ GraphComponent, NodeVisualComponent, LinkVisualComponent, DraggableDirective, ZoomableDirective ],
    exports:      [ GraphComponent, NodeVisualComponent, LinkVisualComponent, DraggableDirective, ZoomableDirective ],
    providers: [ D3Service ],
})
export class D3Module {}
