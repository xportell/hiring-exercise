import { Component, Input, Output, EventEmitter } from '@angular/core';
import { Node } from '../models/node';

@Component({
  selector: '[nodeVisual]',
  template: `
    <svg:g [attr.disabled]="node.disabled" (click)="clickNode()" [attr.transform]="'translate(' + node.x + ',' + node.y + ')'">
      <svg:circle
          class="node"
          [attr.fill]="node.color"
          cx="0"
          cy="0"
          [attr.r]="node.r">
      </svg:circle>
      <svg:text
          class="node-name"
          [attr.font-size]="node.fontSize">
        {{node.value}}
      </svg:text>
      <svg:g *ngIf="node.index === 0 && node.disabled">
        <svg:circle
            [attr.fill]="'white'" fill-opacity="0.5" stroke="white" stroke-width="0" cx="8" cy="35" r="8" >
              <animateTransform attributeName="transform" dur="1s" type="rotate" from="0" to="360" repeatCount="indefinite" />
        </svg:circle>
        <svg:circle
            [attr.fill]="'white'" fill-opacity="0.6" stroke="white" stroke-width="0" cx="8" cy="35" r="9" >
              <animateTransform attributeName="transform" dur="1.5s" type="rotate" from="0" to="360" repeatCount="indefinite" />
        </svg:circle>
        <svg:circle
            [attr.fill]="'white'" fill-opacity="0.7" stroke="white" stroke-width="0" cx="8" cy="35" r="10" >
              <animateTransform attributeName="transform" dur="2s" type="rotate" from="0" to="360" repeatCount="indefinite" />
        </svg:circle>
      </svg:g>
    </svg:g>
  `,
  styleUrls: ['./node-visual.component.css']
})
export class NodeVisualComponent {

  @Input('nodeVisual') node: Node;

  @Output('nodeClick') nodeClick = new EventEmitter();

  clickNode() {
    this.nodeClick.next(this.node);
  }
}
